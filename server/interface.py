import os
import json
import requests

from rq import Queue
from rq.job import Job
from server.worker import conn

from urllib.parse import urljoin

q = Queue(connection=conn)

IOT_HOST = "https://dcp-iotapp-dev.herokuapp.com"
IOT_USER_ID = "rpi1"
IOT_PASSWORD = "rpi1"

def get_token():
    credentials = {'user_id': IOT_USER_ID, 'password': IOT_PASSWORD}
    url = urljoin(IOT_HOST, 'login/token')

    r = requests.post(url, json=credentials)
    r.raise_for_status()
    token = r.json().get('token')

    return token

def _send_records(data, credentials):
    url = urljoin(IOT_HOST, 'api-post/add_records')

    payload = {}
    payload['data'] = data
    payload['credentials'] = credentials

    r = requests.post(url, json=payload)
    r.raise_for_status()
    response = r.text
    try:
        response = json.loads(response)
    except:
        print('redirected')
    
    try:
        message = response.get('message')
    except AttributeError:
        print('no message')
    else:
        return message  

def send_records(data, credentials):
    job = q.enqueue_call(
            func=_send_records, args=(data, credentials), result_ttl=5000
        )
    return job.get_id()