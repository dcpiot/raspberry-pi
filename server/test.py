import os
import json
# from json.decoder import JSONDecodeError
import requests
import sys
from urllib.parse import urljoin

import datetime as dt

credentials = {'user_id': 'rpi1', 'password': 'rpi1'}

def create_sample_data(key):
    data = {}
    data['record_time'] = dt.datetime.utcnow().timestamp()
    data['temperature'] = key
    data['heart_rate'] = key
    data['spo2'] = key
    data['device_tag'] = 'd1'
    red = [key] * 30000
    ir = [key] * 30000
    ppg = {'red': red, 'ir': ir}
    data['ppg'] = json.dumps(ppg)
    print(sys.getsizeof(data))
    return data

# def _send_records(data, credentials):
#     payload = {}
#     payload['data'] = data
#     payload['credentials'] = credentials

#     url = urljoin('http://localhost:5000', 'api-post/add_records')
#     print(url)

#     r = requests.post(url, json=payload)
#     r.raise_for_status()
#     response = r.text
#     try:
#         response = json.loads(response)
#     except JSONDecodeError:
#         print('redirected')
    
#     try:
#         message = response.get('message')
#     except AttributeError:
#         print('no message')
#     else:
#         return message  
    

# status = _send_records( create_sample_data(1) , credentials)
# print(status)