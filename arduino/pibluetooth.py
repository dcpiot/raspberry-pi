import os
import sys
import logging
import binascii
import struct

logger = logging.getLogger("BTLE")
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
sh = logging.StreamHandler(sys.stdout)
sh.setFormatter(formatter)
logger.addHandler(sh)

fh = logging.FileHandler('/home/deploy/raspberry-pi/logs/arduino.log')
fh.setFormatter(formatter)
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

from bluepy.btle import Scanner, DefaultDelegate, Peripheral, ADDR_TYPE_RANDOM

bt_addrs = ['00:15:83:00:45:98', '00:15:83:00:86:72']
HANDLE = 1
scanner = Scanner(0)

while True:
    logger.info('Scanning...')
    
    devices = scanner.scan(2)
    for d in devices:
        logger.info("found device with address : %r", d.addr)
        
        if d.addr in bt_addrs:
            p = Peripheral(d, addrType=ADDR_TYPE_RANDOM)
            logger.info("connected")

            try:
                p.writeCharacteristics(handle=HANDLE, withResponse=True)
                logger.info("sent charateristic to handle %r", HANDLE)

                ch = p.readCharacteristic(handle=HANDLE)
                logger.info("obtained charateristic from handle %r", DATA_UUID)

                if (ch.supportsRead()):
                    val = binascii.b2a_hex(ch.read())
                    val = binascii.unhexlify(val)
                    val = struct.unpack('f', val)
                    logger.info("obtained: %r", val)
            finally:
                conn.disconnect()
            