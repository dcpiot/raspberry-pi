apt-get install -y python3 python3-pip redis-server supervisor
apt-get install python3-serial libglib2.0-dev
pip3 install -r requirements.txt

mkdir /var/log/pi-server
cp ./config/server.conf /etc/supervisor/conf.d/server.conf

mkdir /home/deploy
rm -rf /home/deploy/raspberry-pi
cp -R . /home/deploy/raspberry-pi

supervisorctl reread
supervisorctl update


